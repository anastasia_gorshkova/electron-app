export enum FORMAT {
  PNG = 'png',
  JPG = 'jpg',
  PDF = 'pdf',
}

export type FormatMap<T> = {
  [key in FORMAT]: T;
};
