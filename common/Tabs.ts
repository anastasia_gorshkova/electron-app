export enum TAB_ID {
  TABLE = '/table',
  IMAGE = '/image',
  CHART = '/chart',
}

export const TAB_LIST = Object.keys(TAB_ID).map((key) => TAB_ID[key as keyof typeof TAB_ID]);

export type TabMap<T> = {
  [key in TAB_ID]: T;
};

export const TAB_LABELS: TabMap<string> = {
  '/table': 'Таблица',
  '/image': 'Изображения',
  '/chart': 'Графики',
};

export type TabsState = {
  tabIds: TAB_ID[];
  activeTab?: TAB_ID;
};
