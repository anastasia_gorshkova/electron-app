const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

module.exports = (env = {}) => {
  if (env.production) {
    process.env.NODE_ENV = 'production';
  }
  return {
    target: 'electron-renderer',
    entry: './renderer/index.tsx',
    output: {
      path: env.production ? `${__dirname}/build` : `${__dirname}/watch`,
      filename: 'renderer.js',
    },
    devtool: env.production ? '' : 'inline-source-map',
    node: {
      __dirname: false,
      __filename: false,
    },
    resolve: {
      extensions: ['*', '.js', '.jsx', '.ts', '.tsx'],
      alias: {
        renderer: path.resolve(__dirname, 'renderer/'),
        services: path.resolve(__dirname, 'services/'),
        common: path.resolve(__dirname, 'common/'),
      },
    },
    module: {
      rules: [
        {
          test: /\.(ts|js)x?$/,
          exclude: /node_modules/,
          use: [{ loader: 'ts-loader' }],
        },
        {
          test: /\.(woff|woff2)$/,
          exclude: /node_modules/,
          loader: 'file-loader',
        },
        {
          test: /\.css$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                modules: false,
                url: false,
                importLoaders: 1,
                sourceMap: !env.production,
              },
            },
          ],
        },
        {
          test: /fontkit[\/\\]index.js$/,
          enforce: 'post',
          loader: 'transform-loader?brfs',
        },
        {
          test: /unicode-properties[\/\\]index.js$/,
          enforce: 'post',
          loader: 'transform-loader?brfs',
        },
        {
          test: /linebreak[\/\\]src[\/\\]linebreaker.js/,
          enforce: 'post',
          loader: 'transform-loader?brfs',
        },
      ],
    },
    plugins: [new MiniCssExtractPlugin({ filename: 'bundle.css' })],
    externals: ['electron'],
  };
};
