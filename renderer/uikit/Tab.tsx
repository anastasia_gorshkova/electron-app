import React, { FunctionComponent, useCallback, MouseEvent, ButtonHTMLAttributes, AnchorHTMLAttributes } from 'react';
import { styled } from 'renderer/styled';
import { TAB_ID } from 'common/Tabs';
import { Close, Window } from './Icon';

export interface IProps {
  onCtrlClick?: (to: TAB_ID) => void;
  onClick: (to: TAB_ID) => void;
  onRemove?: (to: TAB_ID) => void;
  to: TAB_ID;
  className?: string;
}

export const TabButton = styled('button')<ButtonHTMLAttributes<HTMLButtonElement>>`
  display: inline-block;
  font-weight: normal;
  white-space: nowrap;
  text-align: center;
  border: none;
  height: 16px;
  font-size: 16px;
  outline: 0;
  background-color: transparent;
  color: ${({ theme }) => theme.colors.primaryText};
  box-shadow: none;
  opacity: 0;
  padding: 0;
  padding-left: 12px;
  cursor: pointer;
`;

export const TabLink = styled('a')<AnchorHTMLAttributes<HTMLAnchorElement>>`
  display: inline-flex;
  white-space: nowrap;
  text-align: center;
  border: none;
  text-decoration: none;
  border-bottom: 1px solid ${({ theme }) => theme.colors.primaryBg};
  font-size: 16px;
  outline: 0;
  background-color: ${({ theme }) => theme.colors.secondaryBg};
  color: ${({ theme }) => theme.colors.defaultText};
  box-shadow: none;
  cursor: pointer;
  padding: 12px;

  & + & {
    border-left: 1px solid ${({ theme }) => theme.colors.primaryBg};
  }

  &.active {
    background-color: ${({ theme }) => theme.colors.primaryBg};
    color: ${({ theme }) => theme.colors.primaryText};
  }
  &:hover,
  &.active {
    button {
      opacity: 1;
    }
  }
`;

export const Tab: FunctionComponent<IProps> = ({ onCtrlClick, onClick, onRemove, children, to, ...props }) => {
  const handleClick = useCallback(
    (e: MouseEvent<HTMLAnchorElement>) => {
      e.stopPropagation();
      e.preventDefault();
      onClick(to);
    },
    [onClick, to],
  );
  const handleNewWindow = useCallback(
    (e: MouseEvent<HTMLButtonElement>) => {
      if (onCtrlClick) {
        e.stopPropagation();
        e.preventDefault();
        onCtrlClick(to);
      }
    },
    [onCtrlClick, to],
  );
  const handleRemove = useCallback(
    (e: MouseEvent<HTMLButtonElement>) => {
      if (onRemove) {
        e.stopPropagation();
        e.preventDefault();
        onRemove(to);
      }
    },
    [onRemove, to],
  );

  return (
    <TabLink {...props} onClick={handleClick}>
      {children}
      {onCtrlClick ? (
        <TabButton onClick={handleNewWindow}>
          <Window width={16} height={16} />
        </TabButton>
      ) : null}
      {onRemove ? (
        <TabButton onClick={handleRemove}>
          <Close width={16} height={16} />
        </TabButton>
      ) : null}
    </TabLink>
  );
};
