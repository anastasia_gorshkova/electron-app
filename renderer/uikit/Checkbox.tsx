import React, { useCallback, ChangeEvent, FunctionComponent } from 'react';
import ThreeStateCheckbox from 'react-three-state-checkbox';

export interface IProps {
  checked: boolean;
  indeterminate?: boolean;
  className?: string;
  style?: React.CSSProperties;
  disabled?: boolean;
  onChange?: (id: string, e: ChangeEvent<HTMLInputElement>) => void;
  id: string;
}

export const Checkbox: FunctionComponent<IProps> = ({ onChange, id, ...props }) => {
  const handleChange = useCallback((e: ChangeEvent<HTMLInputElement>) => onChange && onChange(id, e), [onChange, id]);
  return <ThreeStateCheckbox {...props} onChange={handleChange} />;
};
