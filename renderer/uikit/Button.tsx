import { ButtonHTMLAttributes } from 'react';
import { styled, IDynamicStyles } from 'renderer/styled';
import { css } from '@emotion/core';

export interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  size?: 'large' | 'default' | 'small';
  styles?: 'primary' | 'default' | 'link' | 'icon';
}

const buttonStyle: IDynamicStyles<IProps> = ({ styles, theme }) => {
  switch (styles) {
    case 'primary':
      return css`
        background-color: ${theme.colors.primary};
        color: ${theme.colors.secondaryText};
        border-color: ${theme.colors.primary};
        box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
        &:active {
          color: ${theme.colors.secondaryText};
          background-color: ${theme.colors.primaryDark};
          border-color: ${theme.colors.primaryDark};
        }
        &:hover {
          color: ${theme.colors.secondaryText};
          background-color: ${theme.colors.primaryLight};
          border-color: ${theme.colors.primaryLight};
        }
      `;
    case 'link':
      return css`
        background-color: transparent;
        color: ${theme.colors.primary};
        border-color: transparent;
        box-shadow: none;
        &:active {
          color: ${theme.colors.primaryDark};
          background-color: transparent;
          border-color: transparent;
        }
        &:hover {
          color: ${theme.colors.primaryLight};
          background-color: transparent;
          border-color: transparent;
        }
      `;
    case 'icon':
      return css`
        background-color: transparent;
        color: ${theme.colors.defaultText};
        border-color: transparent;
        box-shadow: none;
        padding: 0;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        &:active,
        &.active {
          color: ${theme.colors.defaultText};
          background-color: ${theme.colors.defaultBg};
          border-color: ${theme.colors.defaultBg};
        }
        &:hover {
          color: ${theme.colors.primaryLight};
          background-color: transparent;
          border-color: transparent;
        }
      `;
    default:
      return css`
        background-color: ${theme.colors.secondaryText};
        color: ${theme.colors.defaultText};
        border-color: ${theme.colors.defaultBg};
        box-shadow: 0 2px 0 rgba(0, 0, 0, 0.015);
        &:active {
          color: ${theme.colors.primaryDark};
          background-color: ${theme.colors.secondaryText};
          border-color: ${theme.colors.primaryDark};
        }
        &:hover {
          color: ${theme.colors.primaryLight};
          background-color: ${theme.colors.secondaryText};
          border-color: ${theme.colors.primaryLight};
        }
      `;
  }
};

const buttonSize: IDynamicStyles<IProps> = ({ size, styles }) => {
  switch (size) {
    case 'large':
      return styles === 'icon'
        ? css`
            height: 40px;
            width: 40px;
          `
        : css`
            height: 40px;
            font-size: 16px;
          `;
    case 'small':
      return styles === 'icon'
        ? css`
            height: 24px;
            width: 24px;
          `
        : css`
            height: 24px;
            font-size: 14px;
          `;
    default:
      return styles === 'icon'
        ? css`
            height: 32px;
            width: 32px;
          `
        : css`
            height: 32px;
            font-size: 14px;
          `;
  }
};

export const Button = styled('button')<IProps>`
  display: inline-flex;
  align-items: center;
  box-sizing: border-box;
  font-weight: normal;
  white-space: nowrap;
  text-align: left;
  border: 1px solid transparent;
  cursor: pointer;
  transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  padding: 0 16px;
  border-radius: 4px;
  outline: 0;
  &:active {
    box-shadow: none;
  }
  &:hover {
    text-decoration: none;
  }
  &[disabled] {
    pointer-events: none;
    cursor: not-allowed;
    opacity: 0.5;
  }

  ${buttonStyle};
  ${buttonSize};

  > svg:first-child {
    padding-right: 8px;
  }
`;

Button.defaultProps = {
  styles: 'default',
  type: 'button',
};
