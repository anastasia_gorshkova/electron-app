import { HTMLAttributes } from 'react';
import { styled } from 'renderer/styled';

export const Toolbar = styled('div')<HTMLAttributes<HTMLElement>>`
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.09);
  padding: 16px;
  background-color: ${({ theme }) => theme.colors.primaryBg};
  display: flex;
  align-items: center;
  > button,
  > label {
    margin: 0 8px;
  }
`;

export default Toolbar;
