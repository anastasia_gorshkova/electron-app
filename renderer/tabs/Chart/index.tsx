import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Chart from './components/Chart';

const App: React.FC = () => (
  <Provider store={store}>
    <Chart />
  </Provider>
);

export default App;
