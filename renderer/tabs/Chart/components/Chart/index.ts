import { connect } from 'react-redux';
import { ChartState } from '../../store/reducers';
import { saveFile } from '../../store/actions';
import { Chart, IStateProps, IDispatchProps } from './Chart';

const mapStateToProps = (state: ChartState): IStateProps => ({
  data: state.data,
  data01: state.data01,
  data02: state.data02,
});

const mapDispatchToProps: IDispatchProps = {
  saveFile,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chart);
