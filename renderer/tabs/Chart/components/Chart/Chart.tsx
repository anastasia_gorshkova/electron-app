import React, { FunctionComponent, HTMLAttributes, useCallback, MutableRefObject, useRef } from 'react';
import domtoimage from 'dom-to-image';
import {
  AreaChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Area,
  BarChart,
  Legend,
  Bar,
  LineChart,
  Line,
  PieChart,
  Pie,
} from 'recharts';
import { styled, theme as appTheme } from 'renderer/styled';
import { Button, Icon, Toolbar } from 'renderer/uikit';
import { FORMAT } from 'common/FileFormat';
import { CahrtData, PieData } from '../../mock/people';

export interface IStateProps {
  data: CahrtData[];
  data01: PieData[];
  data02: PieData[];
}

export interface IDispatchProps {
  saveFile: (content: string, format: FORMAT) => void;
}

export type IProps = IStateProps & IDispatchProps;

export const ChartWrapper = styled('div')<HTMLAttributes<HTMLElement>>`
  padding: 16px;
  background-color: ${({ theme }) => theme.colors.defaultBg};
  box-sizing: border-box;
  width: 100%;
  height: calc(100vh - 64px);
  overflow: auto;
  display: flex;
  justify-content: center;
  > div {
    width: 730px;
  }
`;

export const Chart: FunctionComponent<IProps> = ({ data, data01, data02, saveFile }) => {
  const chartsRef: MutableRefObject<HTMLDivElement | null> = useRef(null);

  const handleSavePngClick = useCallback(() => {
    if (chartsRef.current) {
      domtoimage.toPng(chartsRef.current, { bgcolor: appTheme.colors.defaultBg }).then((content) => {
        saveFile(content, FORMAT.PNG);
      });
    }
  }, [saveFile, chartsRef]);

  const handleSaveJpgClick = useCallback(() => {
    if (chartsRef.current) {
      domtoimage.toJpeg(chartsRef.current, { bgcolor: appTheme.colors.defaultBg }).then((content) => {
        saveFile(content, FORMAT.JPG);
      });
    }
  }, [saveFile, chartsRef]);

  const handleSavePdfClick = useCallback(() => {
    if (chartsRef.current) {
      domtoimage.toPng(chartsRef.current).then((content) => {
        saveFile(content, FORMAT.PDF);
      });
    }
  }, [saveFile, chartsRef]);

  return (
    <>
      <Toolbar>
        <Button onClick={handleSavePngClick}>
          <Icon.ImageFile />
          <span>Сохранить PNG</span>
        </Button>
        <Button onClick={handleSaveJpgClick}>
          <Icon.ImageFile />
          <span>Сохранить JPG</span>
        </Button>
        <Button onClick={handleSavePdfClick}>
          <Icon.PdfFile />
          <span>Сохранить PDF</span>
        </Button>
      </Toolbar>
      <ChartWrapper>
        <div ref={chartsRef}>
          <AreaChart width={730} height={250} data={data} margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
            <defs>
              <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
                <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
              </linearGradient>
              <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
                <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
              </linearGradient>
            </defs>
            <XAxis dataKey="name" />
            <YAxis />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Area type="monotone" dataKey="uv" stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)" />
            <Area type="monotone" dataKey="pv" stroke="#82ca9d" fillOpacity={1} fill="url(#colorPv)" />
          </AreaChart>
          <BarChart width={730} height={250} data={data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="pv" fill="#8884d8" />
            <Bar dataKey="uv" fill="#82ca9d" />
          </BarChart>
          <LineChart width={730} height={250} data={data} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line type="monotone" dataKey="pv" stroke="#8884d8" />
            <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
          </LineChart>
          <PieChart width={730} height={250}>
            <Pie data={data01} dataKey="value" nameKey="name" cx="50%" cy="50%" outerRadius={50} fill="#8884d8" />
            <Pie
              data={data02}
              dataKey="value"
              nameKey="name"
              cx="50%"
              cy="50%"
              innerRadius={60}
              outerRadius={80}
              fill="#82ca9d"
              label
            />
          </PieChart>
        </div>
      </ChartWrapper>
    </>
  );
};
