import { FORMAT } from 'common/FileFormat';
import { CHART_ACTION_TYPE } from '../ActionType';

export const saveFile = (content: string, format: FORMAT) => ({
  type: CHART_ACTION_TYPE.SAVE_FILE as CHART_ACTION_TYPE.SAVE_FILE,
  payload: { content, format },
});
