import { CHART_ACTION_TYPE } from '../ActionType';

export * from './chart';

// export type Actions = ReturnType<typeof addTabId> | ReturnType<typeof removeTabId> | ReturnType<typeof initApp>;

export type ChartActions = {
  type: CHART_ACTION_TYPE;
  payload: { [key: string]: any };
};
