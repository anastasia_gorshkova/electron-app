import { CahrtData, PieData, getChartData, getPieData } from '../../mock/people';

export type ChartState = {
  data: CahrtData[];
  data01: PieData[];
  data02: PieData[];
};

export const InitialState: ChartState = {
  data: getChartData(),
  data01: getPieData(),
  data02: getPieData(),
};

/**
 * Adjust the state of the application according to the action.
 * @param state Current state.
 * @param action Action data.
 * @returns New State.
 */
const reducer = (state: ChartState = InitialState): ChartState => state;

export default reducer;
