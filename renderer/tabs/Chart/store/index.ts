import { createStore, applyMiddleware } from 'redux';
import { chartManager } from 'services/ChartManager';
import RootReducer from './reducers';

const store = createStore(RootReducer, applyMiddleware(chartManager));

export default store;
