const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

export type CahrtData = {
  name: string;
  uv: number;
  pv: number;
  amt: number;
};

export type PieData = {
  name: string;
  value: number;
};

export function getChartData() {
  return alphabet.split('').map((id) => ({
    name: `Page ${id}`,
    uv: Math.floor(Math.random() * 1000),
    pv: Math.floor(Math.random() * 1000),
    amt: Math.floor(Math.random() * 1000),
  }));
}

export function getPieData() {
  return alphabet
    .split('')
    .slice(0, 5)
    .map((id) => ({
      name: `Page ${id}`,
      value: Math.floor(Math.random() * 1000),
    }));
}
