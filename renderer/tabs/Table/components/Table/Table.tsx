import React, { useMemo, useState, useCallback, FunctionComponent, HTMLAttributes } from 'react';
import { styled } from 'renderer/styled';
import ReactTable, { Filter, ReactTableFunction, Column, CellInfo } from 'react-table';
import 'react-table/react-table.css';
import { Checkbox, Icon, Button, Toolbar } from 'renderer/uikit';
import { DEFAULT_PAGE_SIZE, statusColors, Person, statusValues } from '../../mock/people';

export const Wrapper = styled('div')<HTMLAttributes<HTMLElement>>`
  padding: 0;
  background-color: ${({ theme }) => theme.colors.defaultBg};
  width: 100%;
  height: calc(100vh - 120px);
  display: flex;
  overflow: auto;
  > div {
    width: 100%;
  }
`;

type Value = string | number;

type Row = {
  [key: string]: Value;
};

type FilterParams = {
  column: Column;
  filter: Filter;
  onChange: ReactTableFunction;
  key?: string;
};

// Filter Methods

const searchString = (filter: Filter, row: Row) => {
  const str = (row[filter.id] as string).toLowerCase();
  return str.includes(filter.value.toLowerCase());
};

const isStringsEqual = (filter: Filter, row: Row) => String(row[filter.id]) === filter.value;

// Utils

const sum = (vals: number[]) => vals.reduce((prev, current) => prev + current, 0);

const getChecked = (items: Row[], checkedList: string[], getValue?: (i: Row) => Value) => {
  let checked = true;
  let indeterminate = false;
  let isPreviousChecked;
  for (let i = 0; i < items.length; i += 1) {
    const id = getValue ? getValue(items[i]) : items[i];
    const isCurrentChecked = checkedList.includes(String(id));
    if (typeof isPreviousChecked === 'boolean' && isPreviousChecked !== isCurrentChecked) {
      indeterminate = true;
    }
    checked = checked && isCurrentChecked;
    isPreviousChecked = isCurrentChecked;
  }
  return { checked, indeterminate };
};

const joinArrays = (array1: string[], array2: string[]) => {
  const resultArray = [...array1];
  array2.forEach((item) => {
    if (!array1.includes(item)) {
      resultArray.push(item);
    }
  });
  return resultArray;
};

const diffArrays = (array1: string[], array2: string[]) => array1.filter((item) => !array2.includes(item));

// Component

export interface IProps {
  people: Person[];
}

export const Table: FunctionComponent<IProps> = ({ people }) => {
  const [pivot, setPivot] = useState(false);
  const [checkedList, setChecked] = useState<string[]>([]);
  const togglePivot = useCallback(() => setPivot(!pivot), [setPivot, pivot]);
  const toggleChecked = useCallback(
    (id: string) => setChecked(checkedList.includes(id) ? checkedList.filter((c) => c !== id) : [...checkedList, id]),
    [setChecked, checkedList],
  );

  const { indeterminate, checked } = useMemo(() => getChecked(people, checkedList, (p) => p.id), [checkedList, people]);

  const toggleAllChecked = useCallback(() => setChecked(checked ? [] : people.map((p) => p.id)), [
    setChecked,
    checked,
    people,
  ]);

  const togglePivotChecked = useCallback(
    (list, e) => setChecked(e.target.checked ? joinArrays(checkedList, list) : diffArrays(checkedList, list)),
    [setChecked, checkedList],
  );

  const columns = useMemo(
    () => [
      {
        Header: '',
        columns: [
          {
            Header: () => (
              <Checkbox indeterminate={indeterminate} checked={checked} onChange={toggleAllChecked} id="all" />
            ),
            Cell: (row: CellInfo) => (
              <Checkbox
                checked={checkedList.includes(row.value)}
                indeterminate={false}
                onChange={toggleChecked}
                id={row.value}
              />
            ),
            accessor: 'id',
            filterMethod: searchString,
            filterable: false,
            sortable: false,
            width: 30,
            resizable: false,
            aggregate: (vals: (string | string[])[]) => vals.reduce((acc: string[], val) => acc.concat(val), []),
            Aggregated: (row: CellInfo) => (
              <Checkbox {...getChecked(row.value, checkedList)} onChange={togglePivotChecked} id={row.value} />
            ),
          },
        ],
      },
      {
        Header: '',
        pivot: true,
        show: pivot,
      },
      {
        Header: 'Name',
        columns: [
          {
            Header: 'First Name',
            accessor: 'firstName',
            filterMethod: searchString,
          },
          {
            Header: 'Last Name',
            accessor: 'lastName',
            filterMethod: searchString,
          },
        ],
      },
      {
        Header: 'Info',
        columns: [
          {
            Header: 'Age',
            accessor: 'age',
            aggregate: (vals: number[]) => Math.round(sum(vals) / vals.length),
            Aggregated: (row: CellInfo) => <span>{`${row.value} (avg)`}</span>,
          },
          {
            Header: 'Visits',
            accessor: 'visits',
            aggregate: (vals: number[]) => sum(vals),
            Aggregated: (row: CellInfo) => <span>{`${row.value} (sum)`}</span>,
          },
          {
            Header: () => (
              <span style={{ display: 'inline-flex', alignItems: 'center' }}>
                <Icon.Status width={16} height={16} />
                &nbsp;
                <span>Status</span>
              </span>
            ),
            accessor: 'status',
            width: undefined,
            filterMethod: (filter: Filter, row: Row) => filter.value === 'all' || row[filter.id] === filter.value,
            Filter: ({ filter, onChange }: FilterParams) => (
              <select
                onChange={(event) => onChange(event.target.value)}
                style={{ width: '100%' }}
                value={filter ? filter.value : 'all'}
              >
                <option value="all" key="all">
                  Show All
                </option>
                {statusValues.map((s) => (
                  <option value={s} key={s}>
                    {s}
                  </option>
                ))}
              </select>
            ),
            Cell: (row: CellInfo) => (
              <span>
                {statusColors[row.value] && (
                  <span style={{ color: statusColors[row.value], marginRight: 8 }}>&#x25cf;</span>
                )}
                {row.value}
              </span>
            ),
            Aggregated: () => '',
          },
          {
            Header: 'Profile Progress',
            accessor: 'progress',
            aggregate: (vals: number[]) => Math.round(sum(vals) / vals.length),
            Aggregated: (row: CellInfo) => <span>{`${row.value} (avg)`}</span>,
          },
        ],
      },
    ],
    [checkedList, indeterminate, checked, pivot, toggleAllChecked, toggleChecked, togglePivotChecked],
  );

  return (
    <div>
      <Toolbar>
        <label>{`Выбрано элементов: ${checkedList.length}`}</label>
        <Button onClick={togglePivot}>{pivot ? 'Разгруппировать' : 'Группировать по ФИО'}</Button>
      </Toolbar>
      <Wrapper>
        <ReactTable
          data={people}
          columns={columns}
          filterable={!pivot}
          sortable
          defaultFilterMethod={isStringsEqual}
          className="-striped"
          showPaginationBottom={false}
          defaultPageSize={DEFAULT_PAGE_SIZE}
          minRows={1}
          pivotBy={pivot ? ['firstName', 'lastName'] : undefined}
          key={String(pivot)}
          collapseOnSortingChange={false}
          collapseOnPageChange={false}
          collapseOnDataChange={false}
        />
      </Wrapper>
    </div>
  );
};
