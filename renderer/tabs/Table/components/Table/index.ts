import { connect } from 'react-redux';
import { FileState } from '../../store/reducers';
import { Table, IProps } from './Table';

const mapStateToProps = (state: FileState): IProps => ({
  people: state.people,
});

export default connect(mapStateToProps)(Table);
