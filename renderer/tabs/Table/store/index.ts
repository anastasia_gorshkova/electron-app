import { createStore, applyMiddleware } from 'redux';
import { tabManager } from 'services/TabManager';
import RootReducer from './reducers';

const store = createStore(RootReducer, applyMiddleware(tabManager));

export default store;
