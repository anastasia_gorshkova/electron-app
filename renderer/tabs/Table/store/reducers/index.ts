import getPeopleList, { Person, DEFAULT_PAGE_SIZE } from '../../mock/people';

export type FileState = {
  people: Person[];
};

export const InitialState: FileState = {
  people: getPeopleList(DEFAULT_PAGE_SIZE),
};

/**
 * Adjust the state of the application according to the action.
 * @param state Current state.
 * @param action Action data.
 * @returns New State.
 */
const reducer = (state: FileState = InitialState): FileState => state;

export default reducer;
