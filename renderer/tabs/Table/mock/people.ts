export const DEFAULT_PAGE_SIZE = 200;
export const statusValues = ['relationship', 'complicated', 'single'];
export const statusColors: { [key: string]: string } = {
  relationship: '#ff2e00',
  complicated: '#ffbf00',
  single: '#57d500',
};
export const firstNames = [
  'Smith',
  'Johnson',
  'Williams',
  'Jones',
  'Brown',
  'Davis',
  'Miller',
  'Wilson',
  'Moore',
  'Taylor',
  'Anderson',
];
export const lastNames = [
  'JAMES',
  'JOHN',
  'ROBERT',
  'MICHAEL',
  'WILLIAM',
  'DAVID',
  'RICHARD',
  'CHARLES',
  'JOSEPH',
  'THOMAS',
  'CHRISTOPHER',
];

export type Person = {
  id: string;
  firstName: string;
  lastName: string;
  age: number;
  visits: number;
  progress: number;
  status: string;
};

const getStatusText = (statusChance: number) => {
  switch (true) {
    case statusChance > 0.66:
      return statusValues[0];
    case statusChance > 0.33:
      return statusValues[1];
    default:
      return statusValues[2];
  }
};

const getId = () =>
  Math.random()
    .toString(36)
    .substr(2, 9);

const newPerson = () => {
  const statusChance = Math.random();
  return {
    id: getId(),
    firstName: firstNames[Math.floor(Math.random() * 10)],
    lastName: lastNames[Math.floor(Math.random() * 10)],
    age: Math.floor(Math.random() * 30),
    visits: Math.floor(Math.random() * 100),
    progress: Math.floor(Math.random() * 100),
    status: getStatusText(statusChance),
  };
};

export default function getPeopleList(len = 200) {
  return [...Array(len)].map(() => newPerson());
}
