import { ComponentType } from 'react';
import Table from 'renderer/tabs/Table';
import ImageEditor from 'renderer/tabs/ImageEditor';
import Chart from 'renderer/tabs/Chart';
import { TabMap } from 'common/Tabs';

export const TAB_COMPONENTS: TabMap<ComponentType<any>> = {
  '/table': Table,
  '/image': ImageEditor,
  '/chart': Chart,
};
