import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Editor from './components/Editor';

const App: React.FC = () => (
  <Provider store={store}>
    <Editor />
  </Provider>
);

export default App;
