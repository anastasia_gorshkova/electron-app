import { FileActions } from '../actions';
import { FILE_ACTION_TYPE } from '../ActionType';

export type FileState = {
  isSaved: boolean;
  content?: string;
};

export const InitialState: FileState = {
  isSaved: true,
};

/**
 * Adjust the state of the application according to the action.
 * @param state Current state.
 * @param action Action data.
 * @returns New State.
 */
const reducer = (state: FileState = InitialState, { type, payload }: FileActions): FileState => {
  switch (type) {
    case FILE_ACTION_TYPE.SAVE_FILE_SUCCESS:
      return {
        ...state,
        isSaved: true,
      };
    case FILE_ACTION_TYPE.CHANGE_FILE:
      return {
        ...state,
        isSaved: false,
        content: payload.content,
      };

    default:
      return state;
  }
};

export default reducer;
