import { createStore, applyMiddleware } from 'redux';
import { filesManager } from 'services/FilesManager';
import RootReducer from './reducers';

const store = createStore(RootReducer, applyMiddleware(filesManager));

export default store;
