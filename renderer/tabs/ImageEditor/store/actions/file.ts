import { FILE_ACTION_TYPE } from '../ActionType';

export const saveFile = () => ({
  type: FILE_ACTION_TYPE.SAVE_FILE as FILE_ACTION_TYPE.SAVE_FILE,
  payload: {},
});

export const changeFile = (content: string) => ({
  type: FILE_ACTION_TYPE.CHANGE_FILE as FILE_ACTION_TYPE.CHANGE_FILE,
  payload: {
    content,
  },
});

export const init = () => ({
  type: FILE_ACTION_TYPE.INIT as FILE_ACTION_TYPE.INIT,
  payload: {},
});
