import { FILE_ACTION_TYPE } from '../ActionType';

export * from './file';

// export type Actions = ReturnType<typeof addTabId> | ReturnType<typeof removeTabId> | ReturnType<typeof initApp>;

export type FileActions = {
  type: FILE_ACTION_TYPE;
  payload: { [key: string]: any };
};
