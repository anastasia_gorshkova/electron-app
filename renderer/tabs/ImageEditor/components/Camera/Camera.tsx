import React, { useState, useCallback, useRef, MutableRefObject, HTMLAttributes, ButtonHTMLAttributes } from 'react';
import { styled } from 'renderer/styled';
import Webcam from 'react-webcam';
import { Button, Icon } from 'renderer/uikit';

export const CameraOverlay = styled('div')<HTMLAttributes<HTMLElement>>`
  position: fixed;
  display: flex;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 2;
  cursor: pointer;

  video {
    max-width: 100%;
    max-height: 100%;
    margin: auto;
  }
`;

export const CameraButtons = styled('div')<HTMLAttributes<HTMLElement>>`
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  bottom: 16px;
`;

export const CameraButton = styled('button')<ButtonHTMLAttributes<HTMLButtonElement>>`
  display: inline-block;
  box-sizing: border-box;
  font-weight: normal;
  white-space: nowrap;
  text-align: left;
  border: 1px solid white;
  cursor: pointer;
  transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  padding: 8px;
  margin: 0px;
  border-radius: 21px;
  outline: 0;
  background-color: rgba(0, 0, 0, 0.5);
  color: white;
  border-color: white;
  box-shadow: none;
  height: 42px;
  width: 42px;
  text-align: center;
  left: 0;
  &:hover {
    text-decoration: none;
  }
  &:first-child {
    width: 72px;
    margin-right: 24px;
  }
`;

export interface IProps {
  onChange?: (blob: string | null) => void;
}

const videoConstraints = {
  width: 1280,
  height: 720,
  facingMode: 'user',
};

export const Camera: React.FC<IProps> = ({ onChange }) => {
  const [isCameraActive, setCameraActive] = useState(false);

  const activateCamera = useCallback(() => setCameraActive(true), [setCameraActive]);
  const deactivateCamera = useCallback(() => setCameraActive(false), [setCameraActive]);

  const webcamRef: MutableRefObject<Webcam | null> = useRef(null);

  const capture = useCallback(() => {
    if (webcamRef.current && onChange) {
      onChange(webcamRef.current.getScreenshot());
    }
    deactivateCamera();
  }, [webcamRef, onChange, deactivateCamera]);

  return (
    <>
      <Button onClick={activateCamera} styles="icon">
        <Icon.Camera />
      </Button>
      {isCameraActive ? (
        <CameraOverlay>
          <Webcam audio={false} ref={webcamRef} screenshotFormat="image/jpeg" videoConstraints={videoConstraints} />
          <CameraButtons>
            <CameraButton onClick={capture}>
              <Icon.Camera />
            </CameraButton>
            <CameraButton onClick={deactivateCamera}>
              <Icon.Undo />
            </CameraButton>
          </CameraButtons>
        </CameraOverlay>
      ) : null}
    </>
  );
};
