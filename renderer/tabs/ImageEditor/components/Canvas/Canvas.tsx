import React, { useCallback, useState, MutableRefObject, MouseEvent, useRef, useEffect } from 'react';

export interface IProps {
  image: CanvasImageSource;
  onChange: (content: string) => void;
}

export enum CanvasMode {
  Pointer = 'pointer',
  Text = 'text',
  Arrow = 'arrow',
  Crop = 'crop',
}

export const Canvas: React.FC<IProps> = ({ image, onChange }) => {
  const [canvasMode] = useState(CanvasMode.Pointer);
  const canvasRef: MutableRefObject<HTMLCanvasElement | null> = useRef(null);

  useEffect(() => {
    if (canvasRef.current && image) {
      const ctx = canvasRef.current.getContext('2d');
      if (ctx) {
        canvasRef.current.width = image.width as number;
        canvasRef.current.height = image.height as number;
        ctx.drawImage(image, 0, 0);
        onChange(canvasRef.current.toDataURL('image/png'));
      }
    }
  }, [image, canvasRef, onChange]);

  const addText = useCallback((ctx, eventInfo) => {
    ctx.font = '30px Arial';
    ctx.fillText('Hello World', eventInfo.x, eventInfo.y);
  }, []);

  const handleCanvasClick = useCallback(
    (e: MouseEvent<HTMLCanvasElement>) => {
      if (canvasRef.current) {
        const ctx = canvasRef.current.getContext('2d');
        if (ctx) {
          const eventInfo = {
            x: e.pageX - canvasRef.current.offsetLeft,
            y: e.pageY - canvasRef.current.offsetTop,
          };

          switch (canvasMode) {
            case CanvasMode.Text:
              addText(ctx, eventInfo);
              break;
            default:
              break;
          }
        }
      }
    },
    [canvasRef, canvasMode, addText],
  );

  // const setTextMode = useCallback(() => setCanvasMode(CanvasMode.Text), [setCanvasMode]);
  // const setPointerMode = useCallback(() => setCanvasMode(CanvasMode.Pointer), [setCanvasMode]);

  return <canvas ref={canvasRef} onClick={handleCanvasClick} />;
};
