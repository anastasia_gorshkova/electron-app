import React, { useCallback, ChangeEvent, FunctionComponent } from 'react';
import { Button, Icon } from 'renderer/uikit';

const LabelButton = Button.withComponent('label');

export interface IProps {
  onChange?: (blob: string | null) => void;
}

export const File: FunctionComponent<IProps> = ({ onChange }) => {
  const handleFileChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      e.preventDefault();
      const { files } = e.target;
      if (files && files.length) {
        if (onChange) {
          onChange(URL.createObjectURL(files[0]));
        }
        e.target.value = '';
      }
    },
    [onChange],
  );

  return (
    <LabelButton styles="icon">
      <Icon.File />
      <input type="file" style={{ display: 'none' }} accept="image/*" onChange={handleFileChange} />
    </LabelButton>
  );
};
