import { connect } from 'react-redux';
import { saveFile, changeFile, init } from '../../store/actions';
import { FileState } from '../../store/reducers';
import { ImageEditor, IStateProps, IDispatchProps } from './ImageEditor';

const mapStateToProps = (state: FileState): IStateProps => ({
  isSaved: state.isSaved,
});

const mapDispatchToProps: IDispatchProps = {
  saveFile,
  changeFile,
  init,
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageEditor);
