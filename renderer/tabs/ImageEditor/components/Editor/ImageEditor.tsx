import React, { useCallback, FunctionComponent, HTMLAttributes, useEffect, useState } from 'react';
import { Icon, Button, Toolbar } from 'renderer/uikit';
import { styled } from 'renderer/styled';
import Canvas from '../Canvas';
import Camera from '../Camera';
import File from '../File';

export const Wrapper = styled('div')<HTMLAttributes<HTMLElement>>`
  padding: 16px;
  background-color: ${({ theme }) => theme.colors.defaultBg};
  box-sizing: border-box;
  width: 100%;
  height: calc(100vh - 64px);
  display: flex;
  overflow: auto;
  canvas {
    max-width: 100%;
    max-height: 100%;
    margin: auto;
  }
`;

export const Separator = styled('div')<HTMLAttributes<HTMLElement>>`
  display: inline-block;
  width: 1px;
  height: 32px;
  margin: 0 8px;
  background-color: ${({ theme }) => theme.colors.secondaryBg};
`;

export interface IStateProps {
  isSaved: boolean;
}

export interface IDispatchProps {
  saveFile: () => void;
  changeFile: (content: string) => void;
  init: () => void;
}

export type IProps = IStateProps & IDispatchProps;

export const ImageEditor: FunctionComponent<IProps> = ({ isSaved, saveFile, changeFile, init }) => {
  const [image, setImage] = useState<CanvasImageSource | null>(null);

  useEffect(() => {
    init();
  });

  const createImage = useCallback(
    (blob: string | null) => {
      if (blob) {
        const newImg = new Image();
        newImg.src = blob;
        newImg.onload = () => {
          setImage(newImg);
        };
      }
    },
    [setImage],
  );

  const handleFileSave = useCallback(() => {
    saveFile();
  }, [saveFile]);

  return (
    <div>
      <Toolbar>
        <Camera onChange={createImage} />
        <File onChange={createImage} />
        <Separator />
        <Button disabled={!image} styles="icon" className={image ? 'active' : ''}>
          <Icon.Pointer />
        </Button>
        <Button disabled styles="icon">
          <Icon.Crop />
        </Button>
        <Button disabled styles="icon">
          <Icon.Arrow />
        </Button>
        <Button disabled styles="icon">
          <Icon.Text />
        </Button>
        <Separator />
        <Button disabled={!image || isSaved} onClick={handleFileSave} styles="icon">
          <Icon.Save />
        </Button>
        <Button disabled styles="icon">
          <Icon.Undo />
        </Button>
        <Button disabled styles="icon">
          <Icon.Redo />
        </Button>
      </Toolbar>
      <Wrapper>{image ? <Canvas image={image} onChange={changeFile} /> : null}</Wrapper>
    </div>
  );
};
