import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Default from './components/Default';

const App: React.FC = () => (
  <Provider store={store}>
    <Default />
  </Provider>
);

export default App;
