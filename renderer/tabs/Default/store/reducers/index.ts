import { TAB_ID } from 'common/Tabs';
import { WindowActions } from '../actions';
import { WINDOW_ACTION_TYPE } from '../ActionType';

export type WindowState = {
  tabIds: TAB_ID[];
  activeTab?: TAB_ID;
};

export const InitialState: WindowState = {
  tabIds: [],
};

/**
 * Adjust the state of the application according to the action.
 * @param state Current state.
 * @param action Action data.
 * @returns New State.
 */
const reducer = (state: WindowState = InitialState, { payload = {}, type }: WindowActions): WindowState => {
  switch (type) {
    case WINDOW_ACTION_TYPE.SET_TAB_BUTTONS:
      return {
        ...state,
        tabIds: payload.tabIds,
        activeTab: payload.activeTab,
      };

    default:
      return state;
  }
};

export default reducer;
