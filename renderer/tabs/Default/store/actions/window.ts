import { TAB_ID, TabsState } from 'common/Tabs';
import { WINDOW_ACTION_TYPE } from '../ActionType';

export const createNewWindow = (route: TAB_ID) => ({
  type: WINDOW_ACTION_TYPE.CREATE_WINDOW as WINDOW_ACTION_TYPE.CREATE_WINDOW,
  payload: {
    route,
  },
});

export const addTabId = (route: TAB_ID) => ({
  type: WINDOW_ACTION_TYPE.ADD_TAB as WINDOW_ACTION_TYPE.ADD_TAB,
  payload: {
    route,
  },
});

export const removeTabId = (route: TAB_ID) => ({
  type: WINDOW_ACTION_TYPE.REMOVE_TAB as WINDOW_ACTION_TYPE.REMOVE_TAB,
  payload: {
    route,
  },
});

export const setTabButtons = ({ tabIds, activeTab }: TabsState) => ({
  type: WINDOW_ACTION_TYPE.SET_TAB_BUTTONS as WINDOW_ACTION_TYPE.SET_TAB_BUTTONS,
  payload: {
    tabIds,
    activeTab,
  },
});

export const init = () => ({
  type: WINDOW_ACTION_TYPE.INIT as WINDOW_ACTION_TYPE.INIT,
  payload: {},
});
