import { WINDOW_ACTION_TYPE } from '../ActionType';

export * from './window';

// export type Actions = ReturnType<typeof addTabId> | ReturnType<typeof removeTabId> | ReturnType<typeof initApp>;

export type WindowActions = {
  type: WINDOW_ACTION_TYPE;
  payload: { [key: string]: any };
};
