import { createStore, applyMiddleware } from 'redux';
import { windowManager } from 'services/WindowManager';
import RootReducer from './reducers';

const store = createStore(RootReducer, applyMiddleware(windowManager));

export default store;
