import React, { FunctionComponent, HTMLAttributes, ComponentType, useEffect } from 'react';
import { styled } from 'renderer/styled';
import { Button, Icon, Tab } from 'renderer/uikit';
import { TAB_LIST, TAB_ID, TAB_LABELS, TabMap } from 'common/Tabs';

export interface IDispatchProps {
  init: () => void;
  addTabId: (tabId: TAB_ID) => void;
  removeTabId: (tabId: TAB_ID) => void;
  createNewWindow: (tabId: TAB_ID) => void;
}

export interface IStateProps {
  tabIds: TAB_ID[];
  activeTab?: TAB_ID;
}

export type IProps = IDispatchProps & IStateProps;

export const Section = styled('section')<HTMLAttributes<HTMLElement>>`
  padding: 40px;
  max-width: 320px;
`;

export const NavButton = styled(Button)<HTMLAttributes<HTMLElement>>`
  display: flex;
  width: 100%;
  align-items: center;
  & + & {
    margin-top: 16px;
  }
  svg {
    margin-right: 16px;
  }
`;

export const Tabs = styled('nav')<HTMLAttributes<HTMLElement>>`
  background-color: ${({ theme }) => theme.colors.defaultBg};
  padding-top: 8px;
`;

export const Title = styled('h1')<HTMLAttributes<HTMLElement>>`
  font-size: 32px;
  margin-bottom: 24px;
  color: ${({ theme }) => theme.colors.primaryText};
`;

export const TAB_ICONS: TabMap<ComponentType<any>> = {
  '/table': Icon.Table,
  '/image': Icon.Image,
  '/chart': Icon.Chart,
};

export const Default: FunctionComponent<IProps> = ({
  addTabId,
  removeTabId,
  createNewWindow,
  tabIds,
  activeTab,
  init,
}) => {
  useEffect(() => {
    init();
  });
  return tabIds.length ? (
    <Tabs>
      {tabIds.map((id) => (
        <Tab
          to={id}
          key={id}
          onClick={addTabId}
          onCtrlClick={createNewWindow}
          onRemove={removeTabId}
          className={activeTab === id ? 'active' : ''}
        >
          {TAB_LABELS[id]}
        </Tab>
      ))}
    </Tabs>
  ) : (
    <Section>
      <Title>Electon App</Title>
      {TAB_LIST.map((id) => {
        const TabIcon = TAB_ICONS[id];
        return (
          <NavButton size="large" key={id} onClick={() => addTabId(id)}>
            <TabIcon />
            <span>{TAB_LABELS[id]}</span>
          </NavButton>
        );
      })}
    </Section>
  );
};
