import { connect } from 'react-redux';
import { addTabId, createNewWindow, removeTabId, init } from '../../store/actions';
import { WindowState } from '../../store/reducers';
import { Default, IDispatchProps, IStateProps } from './Default';

const mapStateToProps = (state: WindowState): IStateProps => ({
  tabIds: state.tabIds,
  activeTab: state.activeTab,
});

const mapDispatchToProps: IDispatchProps = {
  addTabId,
  createNewWindow,
  removeTabId,
  init,
};

export default connect(mapStateToProps, mapDispatchToProps)(Default);
