import React from 'react';
import { render } from 'react-dom';
import { Router } from 'react-router-dom';
import { ThemeProvider } from 'emotion-theming';
import { theme } from 'renderer/styled';
import { history, basename } from 'services/History';
import { TAB_COMPONENTS } from 'renderer/tabs';
import Default from 'renderer/tabs/Default';
import 'normalize.css';
import './styled/font.css';

const Tab = TAB_COMPONENTS[basename] || Default;
const App: React.FC = () => (
  <Router history={history}>
    <ThemeProvider theme={theme}>
      <Tab />
    </ThemeProvider>
  </Router>
);

window.addEventListener('load', () => render(<App />, document.querySelector('.app')));
