export interface ITheme {
  colors: {
    primary: string;
    primaryDark: string;
    primaryLight: string;
    secondaryBg: string;
    defaultBg: string;
    primaryBg: string;
    secondaryText: string;
    defaultText: string;
    primaryText: string;
  };
}

export const theme: ITheme = {
  colors: {
    primary: '#1890ff',
    primaryDark: '#096dd9',
    primaryLight: '#40a9ff',
    secondaryBg: '#ececec',
    defaultBg: '#f3f3f3',
    primaryBg: '#fff',
    secondaryText: '#fff',
    defaultText: '#6a6a6a',
    primaryText: '#262626',
  },
};

export default theme;
