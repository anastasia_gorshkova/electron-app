import { SerializedStyles } from '@emotion/core';
import { theme, ITheme } from './theme';
import styled from './styled';

export type IStyledProps<P> = P & {
  theme: ITheme;
};

export type IDynamicStyles<P> = (props: IStyledProps<P>) => SerializedStyles;

export { theme, styled };
