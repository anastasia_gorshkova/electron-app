const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = (env = {}) => {
  if (env.production) {
    process.env.NODE_ENV = 'production';
  }
  return {
    target: 'electron-main',
    entry: './main/index.ts',
    output: {
      path: env.production ? `${__dirname}/build` : `${__dirname}/watch`,
      filename: 'main.js',
    },
    devtool: env.production ? '' : 'inline-source-map',
    node: {
      __dirname: false,
      __filename: false,
    },
    resolve: {
      extensions: ['*', '.js', '.jsx', '.ts', '.tsx'],
      alias: {
        main: path.resolve(__dirname, 'main/'),
        common: path.resolve(__dirname, 'common/'),
      },
    },
    module: {
      rules: [
        {
          test: /\.(ts|js)x?$/,
          exclude: /node_modules/,
          use: [{ loader: 'ts-loader' }],
        },
      ],
    },
    plugins: [new CopyPlugin([{ from: 'assets', to: './' }])],
  };
};
