declare module 'svg-to-pdfkit' {
  function SVGToPDF(doc: PDFKit.PDFDocument, svg: string, x: number, y: number): void;
  export = SVGToPDF;
}
