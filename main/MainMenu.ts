import { app, Menu, MenuItemConstructorOptions } from 'electron';
import isDev from 'electron-is-dev';
import { TAB_LABELS, TAB_LIST } from 'common/Tabs';
import { addTab, openDevTools, reloadTab, getCurrentLocation } from './NewWindow';

const { shell } = require('electron');

/**
 * Create a template for the menu.
 * @returns Template.
 */
const createTemplate = (): MenuItemConstructorOptions[] => {
  const isMac = process.platform === 'darwin';
  return [
    ...(isMac
      ? [
          {
            label: app.name,
            submenu: [
              { role: 'about', label: `О ${app.name}` },
              { type: 'separator' },
              { role: 'services', label: 'Службы' },
              { role: 'hide', label: `Скрыть ${app.name}` },
              { role: 'hideothers', label: 'Скрыть остальные' },
              { role: 'unhide', label: 'Показать все' },
              { type: 'separator' },
              { role: 'quit', label: `Выйти из ${app.name}` },
            ],
          },
        ]
      : []),
    {
      label: 'Посмотреть',
      submenu: [
        {
          // role: 'reload',
          label: 'Обновить страницу',
          accelerator: 'CmdOrCtrl+R',
          click() {
            reloadTab();
          },
        },
        ...(isDev
          ? [
              {
                // role: 'toggledevtools',
                label: 'Инструменты разработчика',
                accelerator: isMac ? 'Alt+Cmd+I' : 'Ctrl+Shift+I',
                click() {
                  openDevTools();
                },
              },
            ]
          : []),
        { role: 'togglefullscreen', label: 'Полноэкранный режим' },
        { type: 'separator' },
        ...TAB_LIST.map((tab) => ({
          label: TAB_LABELS[tab],
          click() {
            addTab(tab);
          },
        })),
        { type: 'separator' },
        {
          label: 'Показать текущую ссылку',
          click() {
            getCurrentLocation();
          },
        },
      ],
    },
    {
      role: 'window',
      label: 'Окно',
      submenu: [
        ...(isMac
          ? [
              { role: 'close', label: 'Закрыть' },
              { role: 'minimize', label: 'Свернуть' },
              { role: 'zoom', label: 'Масштаб' },
              { type: 'separator' },
              { role: 'front', label: 'Переместить на передний план' },
            ]
          : [
              { role: 'minimize', label: 'Свернуть' },
              { role: 'close', label: 'Закрыть' },
            ]),
      ],
    },
    {
      role: 'help',
      label: 'Сравка',
      submenu: [
        {
          label: `Справка ${app.name}`,
          click() {
            shell.openExternal('https://electronjs.org');
          },
        },
      ],
    },
  ] as MenuItemConstructorOptions[];
};

/**
 * Create and set main menu.
 */
export const createMainMenu = () => {
  const template = Menu.buildFromTemplate(createTemplate());
  Menu.setApplicationMenu(template);
};

export default createMainMenu;
