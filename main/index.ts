import { app } from 'electron';
import { initializeIpcEvents, releaseIpcEvents } from './IPCEvents';
import { createMainMenu } from './MainMenu';
import { createNewWindow } from './NewWindow';

app.name = 'ElectronApp';

app.on('ready', () => {
  createNewWindow();
  createMainMenu();
  initializeIpcEvents();
});

// app.on('quit', () => {});

app.on('window-all-closed', () => {
  releaseIpcEvents();
  app.quit();
});
