import { BrowserWindow, BrowserView } from 'electron';
import promiseIpc from 'electron-promise-ipc';
import Path from 'path';
import urlParse from 'url-parse';
import { IPC_KEY } from 'common/IPCKey';
import { TAB_ID } from 'common/Tabs';

const { dialog, clipboard } = require('electron');

/**
 * Current window list.
 */

const TABS_HEIGHT = 50;

const currentWindows: Map<number, WindowTabs> = new Map();
const filePath = Path.join(__dirname, 'index.html');

class WindowTabs {
  views: Map<TAB_ID, BrowserView>;

  window: BrowserWindow;

  constructor(window: BrowserWindow) {
    this.window = window;
    this.views = new Map();
  }

  activateView(tabId: TAB_ID) {
    const currentView = this.views.get(tabId) || WindowTabs.createNewView(tabId);
    this.views.set(tabId, currentView);
    this.setBrowserView(currentView);
  }

  async removeView(tabId: TAB_ID, windowTabs?: WindowTabs) {
    const currentView = this.views.get(tabId) || null;
    if (!currentView) {
      return;
    }

    const canDelete = await (windowTabs
      ? windowTabs.getViewByTabId(tabId)
      : promiseIpc.send(IPC_KEY.SAVE_TAB_CONTENT, currentView.webContents));

    if (canDelete) {
      this.deleteView(tabId);
      if (windowTabs) {
        windowTabs.addExistingView(tabId, currentView);
      }
    }
  }

  getViewByTabId(tabId: TAB_ID) {
    return !this.views.get(tabId);
  }

  addExistingView(tabId: TAB_ID, view: BrowserView) {
    this.views.set(tabId, view);
    this.setBrowserView(view);
  }

  getActiveView() {
    const viewsArray = Array.from(this.views);
    for (let i = 0; i < viewsArray.length; i += 1) {
      const activeView = viewsArray[i];
      const parentWindow = BrowserWindow.fromBrowserView(activeView[1]);
      if (parentWindow) {
        return activeView;
      }
    }
    return [];
  }

  private deleteView(tabId: TAB_ID) {
    this.views.delete(tabId);
    const lastView = Array.from(this.views.values())[this.views.size - 1] || null;
    this.setBrowserView(lastView);
  }

  private setBrowserView(view: BrowserView | null) {
    this.window.setBrowserView(view);
    if (view) {
      const [width, height] = this.window.getContentSize();
      view.setBounds({ x: 0, y: TABS_HEIGHT, width, height: height - TABS_HEIGHT });
      view.setAutoResize({ width: true, height: true });
    }
    // this.window.focus();
    // after this.view was changed or this.window.browserView was changed
    const tabIds = Array.from(this.views.keys());
    const activeTab = this.getActiveView()[0];
    this.window.webContents.send(IPC_KEY.SET_TAB_BUTTONS, { tabIds, activeTab });
  }

  static createNewView(tabId: TAB_ID) {
    const view = new BrowserView({
      webPreferences: {
        nodeIntegration: true,
      },
    });
    view.webContents.loadURL(`file://${filePath}#${tabId}`);
    return view;
  }
}

/**
 * Create a window and add it to the list.
 */
export const createNewWindow = () => {
  const newWindow = new BrowserWindow({
    width: 800,
    height: 600,
    minWidth: 800,
    minHeight: 600,
    resizable: true,
    show: false,
    webPreferences: {
      nodeIntegration: true,
    },
  });
  newWindow.maximize();

  const windowId = newWindow.id;
  newWindow.on('closed', () => {
    currentWindows.delete(windowId);
  });
  newWindow.once('ready-to-show', () => {
    newWindow.show();
  });

  return newWindow.loadURL(`file://${filePath}`).then(() => {
    const newWindowTabs = new WindowTabs(newWindow);
    currentWindows.set(newWindow.id, newWindowTabs);
    return newWindowTabs;
  });
};

export const getFocucedWindowTabs = () => {
  const focuced = BrowserWindow.getFocusedWindow();
  if (focuced) {
    return currentWindows.get(focuced.id);
  }
  return null;
};

export const addTab = (route: TAB_ID, parentId?: number) => {
  const parentWindow = parentId && currentWindows.get(parentId);
  const focusedWindow = parentWindow || getFocucedWindowTabs();
  if (focusedWindow) {
    focusedWindow.activateView(route);
  }
};

export const removeTab = (route: TAB_ID, parentId?: number) => {
  const parentWindow = parentId && currentWindows.get(parentId);
  const focusedWindow = parentWindow || getFocucedWindowTabs();
  if (focusedWindow) {
    focusedWindow.removeView(route);
  }
};

export const openDevTools = () => {
  const focusedWindow = getFocucedWindowTabs();
  if (focusedWindow) {
    const active = focusedWindow.getActiveView()[1] || focusedWindow.window;
    active.webContents.openDevTools();
  }
};

export const reloadTab = () => {
  const focusedWindow = getFocucedWindowTabs();
  if (focusedWindow) {
    const active = focusedWindow.getActiveView()[1] || focusedWindow.window;
    active.webContents.reload();
  }
};

export const getCurrentLocation = () => {
  const focusedWindow = getFocucedWindowTabs();
  if (focusedWindow) {
    const active = focusedWindow.getActiveView()[1] || focusedWindow.window;
    const url = urlParse(active.webContents.getURL(), {});
    const hash = url.hash.replace('#', '') || '/';
    clipboard.writeText(hash, 'selection');
    dialog.showMessageBox(focusedWindow.window, {
      message: 'Ссылка была скопирована',
      detail: hash,
    });
  }
};

export const moveTab = (route: TAB_ID, parent: number | WindowTabs, child: number | WindowTabs) => {
  const parentWindow = typeof parent === 'number' ? currentWindows.get(parent) : parent;
  const childWindow = typeof child === 'number' ? currentWindows.get(child) : child;
  if (parentWindow && childWindow) {
    parentWindow.removeView(route, childWindow);
  }
};
