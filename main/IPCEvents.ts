import { ipcMain, IpcMainInvokeEvent } from 'electron';
import { IPC_KEY } from 'common/IPCKey';
import { TAB_ID } from 'common/Tabs';
import { addTab, removeTab, createNewWindow, moveTab } from './NewWindow';

const onCreateNewWindow = (_: IpcMainInvokeEvent, route: TAB_ID, parentId: number) => {
  createNewWindow().then((childWindow) => {
    moveTab(route, parentId, childWindow);
  });
};

const onAddTab = (_: IpcMainInvokeEvent, route: TAB_ID, parentId: number) => {
  addTab(route, parentId);
};

const onRemoveTab = (_: IpcMainInvokeEvent, route: TAB_ID, parentId: number) => {
  removeTab(route, parentId);
};

let initialized = false;

export const initializeIpcEvents = () => {
  if (initialized) {
    return;
  }
  initialized = true;

  ipcMain.handle(IPC_KEY.CREATE_NEW_WINDOW, onCreateNewWindow);
  ipcMain.handle(IPC_KEY.ADD_TAB, onAddTab);
  ipcMain.handle(IPC_KEY.REMOVE_TAB, onRemoveTab);
};

export const releaseIpcEvents = () => {
  if (initialized) {
    ipcMain.removeAllListeners(IPC_KEY.CREATE_NEW_WINDOW);
  }

  initialized = false;
};
