import { Middleware, Dispatch } from 'redux';
import { WindowActions, setTabButtons } from 'renderer/tabs/Default/store/actions';
import { WINDOW_ACTION_TYPE } from 'renderer/tabs/Default/store/ActionType';
import { IPC_KEY } from 'common/IPCKey';
import { TabsState } from 'common/Tabs';

const { ipcRenderer, remote } = window.require('electron');

export const windowManager: Middleware = (store) => (next: Dispatch) => (action: WindowActions): WindowActions => {
  if (action.type === WINDOW_ACTION_TYPE.INIT) {
    ipcRenderer.on(IPC_KEY.SET_TAB_BUTTONS, (_, state: TabsState) => {
      store.dispatch(setTabButtons(state));
    });
  }

  if (action.type === WINDOW_ACTION_TYPE.CREATE_WINDOW) {
    ipcRenderer.invoke(IPC_KEY.CREATE_NEW_WINDOW, action.payload.route, remote.getCurrentWindow().id);
  }
  if (action.type === WINDOW_ACTION_TYPE.ADD_TAB) {
    ipcRenderer.invoke(IPC_KEY.ADD_TAB, action.payload.route, remote.getCurrentWindow().id);
  }
  if (action.type === WINDOW_ACTION_TYPE.REMOVE_TAB) {
    ipcRenderer.invoke(IPC_KEY.REMOVE_TAB, action.payload.route, remote.getCurrentWindow().id);
  }

  return next(action);
};
