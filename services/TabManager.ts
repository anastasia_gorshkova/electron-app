import { Middleware, Dispatch } from 'redux';
import { IPC_KEY } from 'common/IPCKey';

const promiseIpc = window.require('electron-promise-ipc');

export const tabManager: Middleware = (store) => {
  promiseIpc.on(IPC_KEY.SAVE_TAB_CONTENT, () => {
    const { isSaved, content } = store.getState();
    if (isSaved || !content) {
      return true;
    }
    return false;
  });

  return (next: Dispatch) => (action) => next(action);
};
