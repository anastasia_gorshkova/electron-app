import { Middleware, Dispatch } from 'redux';
import { FileActions } from 'renderer/tabs/ImageEditor/store/actions';
import { FILE_ACTION_TYPE } from 'renderer/tabs/ImageEditor/store/ActionType';
import { IPC_KEY } from 'common/IPCKey';
import { saveFileAsPng } from './SaveFile';

const { remote } = window.require('electron');
const promiseIpc = window.require('electron-promise-ipc');

export const filesManager: Middleware = (store) => (next: Dispatch) => (action: FileActions): FileActions => {
  if (action.type === FILE_ACTION_TYPE.INIT) {
    promiseIpc.on(IPC_KEY.SAVE_TAB_CONTENT, () => {
      const { isSaved, content } = store.getState();
      if (isSaved || !content) {
        return true;
      }
      const buttonIndex = remote.dialog.showMessageBoxSync(remote.getCurrentWindow(), {
        type: 'question',
        buttons: ['Не сохранять', 'Отмена', 'Сохранить'],
        title: 'Сохранить файл перед выходом?',
        message: 'Изменения будут потеряны, если вы их не сохраните.',
        cancelId: 0,
      });
      if (buttonIndex === 2) {
        const wasSavedSuccessfuly = saveFileAsPng(content);
        return wasSavedSuccessfuly;
      }
      return buttonIndex !== 1;
    });
  }

  if (action.type === FILE_ACTION_TYPE.SAVE_FILE) {
    const wasSavedSuccessfuly = saveFileAsPng(store.getState().content);
    if (wasSavedSuccessfuly) {
      store.dispatch({ type: FILE_ACTION_TYPE.SAVE_FILE_SUCCESS, payload: {} });
    }
  }
  return next(action);
};
