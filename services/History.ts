import createHashHistory from 'history/createHashHistory';
import { TAB_ID } from 'common/Tabs';

const url = window.location.hash;
export const basename = `/${url.split(/#\/|\//).filter(Boolean)[0] || ''}` as TAB_ID;
export const history = createHashHistory({
  hashType: 'slash',
  basename,
});
