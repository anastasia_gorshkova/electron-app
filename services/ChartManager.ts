import { Middleware, Dispatch } from 'redux';
import { ChartActions } from 'renderer/tabs/Chart/store/actions';
import { CHART_ACTION_TYPE } from 'renderer/tabs/Chart/store/ActionType';
import { IPC_KEY } from 'common/IPCKey';
import { FORMAT } from 'common/FileFormat';
import { saveFileAsPng, saveFileAsJpg, saveImageAsPdf } from './SaveFile';

const promiseIpc = window.require('electron-promise-ipc');

const saveFileAsFormat = (content: string, format: FORMAT) => {
  switch (format) {
    case FORMAT.PNG:
      return saveFileAsPng(content);
    case FORMAT.JPG:
      return saveFileAsJpg(content);
    case FORMAT.PDF:
      return saveImageAsPdf(content);
    default:
      return false;
  }
};

export const chartManager: Middleware = (store) => {
  promiseIpc.on(IPC_KEY.SAVE_TAB_CONTENT, () => {
    const { isSaved, content } = store.getState();
    if (isSaved || !content) {
      return true;
    }
    return false;
  });

  return (next: Dispatch) => (action: ChartActions): ChartActions => {
    if (action.type === CHART_ACTION_TYPE.SAVE_FILE) {
      saveFileAsFormat(action.payload.content, action.payload.format);
    }
    return next(action);
  };
};
