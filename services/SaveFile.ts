import fs from 'fs';
import { FORMAT } from 'common/FileFormat';
import { FileFilter } from 'electron';

const PDFDocument = window.require('pdfkit');
// const SVGtoPDF = window.require('svg-to-pdfkit');

const { remote } = window.require('electron');

export const showSaveFileDialog = (filters: FileFilter[]) =>
  remote.dialog.showSaveDialogSync({
    title: 'Save file',
    buttonLabel: 'Save',
    filters,
  });

export const saveImageFile = (base64Data: string, filePath?: string) => {
  if (filePath) {
    fs.writeFileSync(filePath, base64Data, 'base64');
    return true;
  }
  return false;
};

export const saveFileAsPng = (content: string) => {
  const filePath = showSaveFileDialog([{ name: FORMAT.PNG, extensions: [FORMAT.PNG] }]);
  return saveImageFile(content.replace(/^data:image\/png;base64,/, ''), filePath);
};

export const saveFileAsJpg = (content: string) => {
  const filePath = showSaveFileDialog([{ name: FORMAT.JPG, extensions: [FORMAT.JPG] }]);
  return saveImageFile(content.replace(/^data:image\/jpeg;base64,/, ''), filePath);
};

export const saveImageAsPdf = (image: string) => {
  const filePath = showSaveFileDialog([{ name: FORMAT.PDF, extensions: [FORMAT.PDF] }]);
  if (!filePath) {
    return false;
  }

  const doc = new PDFDocument();
  doc.pipe(fs.createWriteStream(filePath));
  doc.font(`${__dirname}/fonts/roboto-v20-cyrillic_latin-300.woff`);
  // SVGtoPDF(doc, svg, 0, 0);
  doc.image(image, {
    fit: [450, 1000],
    align: 'center',
    valign: 'top',
  });
  doc.end();

  return true;
};
