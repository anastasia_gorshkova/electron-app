## Installation

1. `npm install`

## Development

### Watch

Run the watch files, background complie JavaScript/TypeScript/CSS.

```bash
npm start
```

## Launch application

Launch the Electron app.

```bash
npm run app
```